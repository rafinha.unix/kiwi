package consumers

import (
	"gitlab.com/rafinha.unix/kiwi/decoders"
	"gitlab.com/rafinha.unix/kiwi/repository"
)

func ConsumerOneMessage(m *decoders.MessageHandler) (*decoders.ConsumerReponseMessage, error) {
	client := repository.NewRedisClient()
	returnMessage, err := client.LPop(m.Topic).Result()
	if err != nil {
		return new(decoders.ConsumerReponseMessage), err
	}

	cmessage := decoders.ConsumerReponseMessage{Topic: m.Topic, Message: returnMessage}
	return &cmessage, nil
}
