package producers

import (
	"gitlab.com/rafinha.unix/kiwi/decoders"
	"gitlab.com/rafinha.unix/kiwi/repository"
)

func ProducerOneMessage(m *decoders.MessageHandler) {
	client := repository.NewRedisClient()
	client.RPush(m.Topic, m.Message)
}
