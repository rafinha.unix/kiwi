package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
)

type Config struct {
	Socket   ConfigSocket
	Producer ProducerConfig
	Consumer ConsumerConfig
	Redis    ConfigRedis
}

type ProducerConfig struct {
	ProducerOneMessageCommand string
}

type ConsumerConfig struct {
	ConsumerOneMessageCommand string
}

type ConfigSocket struct {
	EndSocketCommand string
	SocketHostName   string
	SocketPort       int
	SocketTimeout    int
}

type ConfigRedis struct {
	Host     string
	Password string
	Port     int
}

var (
	config *Config
	once   sync.Once
)

func GetConfig() *Config {
	once.Do(func() {
		fmt.Println("PEGOU CONFIG")
		cfgpath := os.Getenv("CONFIG_FILE")
		file, err := ioutil.ReadFile(cfgpath)
		if err != nil {
			fmt.Println("Get Config Error", err.Error())
		}
		json.Unmarshal(file, &config)
	})
	return config
}
