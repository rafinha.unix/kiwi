package main

import (
	"fmt"
	"net"

	"gitlab.com/rafinha.unix/kiwi/config"
	"gitlab.com/rafinha.unix/kiwi/handlers"
)

var cfg = config.GetConfig()

func main() {
	h := fmt.Sprintf("%s:%d", cfg.Socket.SocketHostName, cfg.Socket.SocketPort)
	ln, err := net.Listen("tcp4", h)
	if err != nil {
		fmt.Println("LISTEN ERROR", err.Error())
	}

	fmt.Printf("Listen %s\n", h)

	for {
		c, err := ln.Accept()
		h := handlers.SocketConnection{Conn: c}
		fmt.Println(" New Connection")
		if err != nil {
			fmt.Println("Accept Error", err.Error())
		}
		go h.SocketHandling()
	}

}
