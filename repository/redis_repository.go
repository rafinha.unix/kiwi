package repository

import (
	"fmt"
	"sync"

	"github.com/go-redis/redis"
	"gitlab.com/rafinha.unix/kiwi/config"
)

var cfg = config.GetConfig()

var (
	client *redis.Client
	once   sync.Once
)

func NewRedisClient() *redis.Client {
	var h = fmt.Sprintf("%s:%d", cfg.Redis.Host, cfg.Redis.Port)
	once.Do(func() {
		client = redis.NewClient(&redis.Options{
			Addr:     h,
			Password: cfg.Redis.Password,
			DB:       0,
		})
	})

	return client
}
