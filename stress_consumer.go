package main

import "net"
import "fmt"
import "time"

func main() {

	x := 0
	for x < 50 {
		go func() {
			conn, _ := net.Dial("tcp4", "127.0.0.1:7777")
			i := 0
			for i < 1000 {
				func() {
					message := "topic->new_topic_x->token->thread_token->message->{teste: AEEMESSAGE}->command->consumerOneMessageCommand\n"
					conn.Write([]byte(message))
					buff := make([]byte, 1024)
					n, _ := conn.Read(buff)
					fmt.Printf("Receive: %s\n", buff[:n])
				}()
				time.Sleep(1 * time.Millisecond)
				i += 1
			}
		}()
		x += 1
	}
	time.Sleep(500 * time.Second)
}
