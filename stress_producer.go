package main

import (
	"fmt"
	"math/rand"
	"net"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

func RandStringRunes(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}

func main() {
	x := 0
	for x < 10 {
		go func() {
			conn, _ := net.Dial("tcp4", "127.0.0.1:7777")
			i := 0
			for i < 100000 {
				func() {
					m := RandStringRunes(10)
					message := fmt.Sprintf("topic->new_topic_x->token->thread_token->message->%s->command->producerOneMessageCommand\n", m)
					fmt.Println("Send Message -> ", message)
					conn.Write([]byte(message))
				}()
				i += 1
				time.Sleep(1 * time.Millisecond)
			}
		}()
		x += 1
	}
	time.Sleep(500 * time.Second)
}
