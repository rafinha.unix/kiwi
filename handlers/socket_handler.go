package handlers

import (
	"bufio"
	"fmt"
	"net"
	"time"

	"gitlab.com/rafinha.unix/kiwi/config"
	"gitlab.com/rafinha.unix/kiwi/consumers"
	"gitlab.com/rafinha.unix/kiwi/decoders"
	"gitlab.com/rafinha.unix/kiwi/producers"
)

type SocketHandler interface {
	SocketHandling(c *SocketConnection)
	MessageHandling(c *SocketConnection)
}

type SocketConnection struct {
	Conn    net.Conn
	Message decoders.MessageHandler
}

var cfg = config.GetConfig()

func (c *SocketConnection) SocketHandling() {
	reader := bufio.NewReader(c.Conn)
	for {
		m, err := reader.ReadBytes('\n')
		if err != nil {
			if checkSocketTimeout(err) {
				fmt.Println("Timeout Error", err.Error())
				defer c.Conn.Close()
				break
			}
		}

		if m != nil {
			c.Message = decoders.DecodeMessageHandler(string(m))
		}

		if c.Message.Command != "" {
			c.Conn.SetReadDeadline(time.Now().Add(time.Duration(cfg.Socket.SocketTimeout) * time.Second))
			c.MessageHandling()
		}
	}
}

func (c *SocketConnection) MessageHandling() {
	switch c.Message.Command {
	case "producerOneMessageCommand":
		go producers.ProducerOneMessage(&c.Message)
	case "consumerOneMessageCommand":
		w := bufio.NewWriter(c.Conn)
		r, _ := consumers.ConsumerOneMessage(&c.Message)
		mj, err := decoders.DecodeConsumerResponseMessage(r)
		if err != nil {
			fmt.Println("Error convert to json consumer msg", err.Error())
			return
		}
		w.Write([]byte(mj))
		w.Flush()
	}
}

func checkSocketTimeout(err error) bool {
	if neterr, ok := err.(net.Error); ok && neterr.Timeout() {
		return true
	}
	return false
}
