package decoders

import (
	"encoding/json"
	"errors"
	"fmt"
	"regexp"
	"strings"
)

type MessageHandler struct {
	Token, Topic, Message, Command string
}

type ConsumerReponseMessage struct {
	Topic, Message string
}

var regexDecodeMessage, _ = regexp.Compile(`topic->(?P<topic>[^->]*?)->token->(?P<token>[^->]*?)->message->(?P<message>[^->]*?)->command->(?P<command>[^$]*?)$`)

func DecodeMessageHandler(m string) MessageHandler {
	match := regexDecodeMessage.FindStringSubmatch(strings.TrimSpace(m))
	r := make(map[string]string)
	for i, name := range regexDecodeMessage.SubexpNames() {
		if i != 0 {
			if len(match) > 0 {
				r[name] = match[i]
			} else {
				fmt.Println(m)
			}
		}
	}
	var msg = MessageHandler{Topic: r["topic"],
		Token: r["token"], Message: r["message"], Command: r["command"]}
	return msg
}

func DecodeConsumerResponseMessage(m *ConsumerReponseMessage) (string, error) {
	messageJson, err := json.Marshal(&m)
	if err != nil {
		return string(messageJson), errors.New(err.Error())
	}

	return string(messageJson), nil
}
