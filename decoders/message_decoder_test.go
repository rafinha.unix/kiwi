package decoders

import (
	"testing"
)

const producerMessage = "topic->new_topic303->token->thread_token->message->testMessage->command->producerOneMessageCommand\n"

func TestDecodeMessageHandler(t *testing.T) {
	m := DecodeMessageHandler(producerMessage)
	if m.Command != "producerOneMessageCommand" {
		t.Errorf("m.Command expected: %s, actual: %s", "producerOneMessageCommand", m.Command)
	}

	if m.Message != "testMessage" {
		t.Errorf("m.Message expected: %s, actual: %s", "testMessage", m.Message)
	}

	if m.Topic != "new_topic303" {
		t.Errorf("m.Topic expected: %s, actual: %s", "new_topic303", m.Topic)
	}

	if m.Token != "thread_token" {
		t.Errorf("m.Token expected: %s, actual: %s", "thread_token", m.Token)
	}
}
